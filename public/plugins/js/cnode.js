/**
 * Created by wuwanyu on 2016/6/22.
 */
var adminModule = angular.module('admin-app' ,['ui.router','ngPagination']);

adminModule.config(function($stateProvider, $urlRouterProvider) {

    // For any unmatched url, redirect to /state1
    $urlRouterProvider
        .when('/list', ['$state', function ($state) {  //为active.ejs的ui-view设置默认值
            $state.go('list');
        }])
        .otherwise("/list");

    // Now set up the states
    $stateProvider
        .state('list', {  //首页
            url: "/list",
            templateUrl: "list1",
            controller: "list.controller"
        })
        .state('info', {  //详情
            url: "/info?article_id",
            templateUrl: "info1",
            controller: "info.controller"
        })
        ;

});

//admin全局controller
adminModule.controller('global.controller', ['$scope','$http','$location',function ($scope, $http, $location) {

    console.log("global controller");


    //header 切换导航
    $("header ul.nav li").click(function(){
        var obj = $(this);

        //遍历删除cur
        $("header ul.nav li").each(function () {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
            }
        });

        //添加cur
        obj.addClass("active");

    });


}]);


adminModule.filter( "toLocaleString", function() {
    var filterfun = function(code) {
        var result = new Date(parseInt(code)).toLocaleString() ;

        return result;
    };
    return filterfun;
});

/**
 * Created by wuwanyu on 2016/6/22.
 */
/**
 * Created by wuwy on 2016/4/22.
 */
adminModule.controller('info.controller',['$scope','$stateParams','articleService',function($scope,$stateParams,articleService) {


    var article_id = $stateParams.article_id;

    console.log("article_id:",article_id);

    articleService.getArticleInfo({article_id:article_id},function(response){
        console.log("response.result:",response.result);
        $scope.info = response.result;

    })




}] );

/**
 * Created by wuwanyu on 2016/6/22.
 */
adminModule.controller('list.controller', ['$scope','$state','$location','articleService',function ($scope, $state, $location,articleService) {

    console.log("in list controller");

    $scope.paginationConf={
        currentPage: 1,  //当前页码
        totalItems: 400, //数据总量
        itemsPerPage: 12,  //每页显示数据
        pagesLength: 10,   //显示页码
        onChange: function(){  //数据变化时，触发
            $scope.onPageChange(this.currentPage,this.itemsPerPage);
        }
    };


    articleService.getArticleList(null,function(response){

        console.log("response.result:",response.result);

        $scope.list = response.result;

    });

    $scope.toArticleInfo = function(article_id){
        $state.go('info',{article_id:article_id});
    }

    //首页文字类型 切换导航
    $(".container ul.nav-pills li").click(function(){
        var obj = $(this);

        //遍历删除cur
        $(".container .panel-heading ul.nav-pills li").each(function () {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
            }
        });

        //添加cur
        obj.addClass("active");

    });


}]);
/**
 * Created by wuwanyu on 2016/6/22.
 */

adminModule.service('articleService',['$http',function($http){

    //getList
    this.getArticleList = function(params,cb){
        angularHttpGet($http,"/cnode/api/article/getList",params,cb)


    }


    //getInfo
    this.getArticleInfo = function(params,cb){
        angularHttpGet($http,"/cnode/api/article/getInfo",params,cb)
    }


//angularJs https get������װ
    function angularHttpGet($http,url,params,callBack){
        $http({
            method  : 'GET',
            url     : url,
            params : params
        })
            .success(function(data) {
                callBack(data);
            });
    }



}] );



