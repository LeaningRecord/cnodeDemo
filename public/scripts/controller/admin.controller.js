/**
 * Created by wuwanyu on 2016/6/22.
 */
var adminModule = angular.module('admin-app' ,['ui.router','ngPagination']);

adminModule.config(function($stateProvider, $urlRouterProvider) {

    // For any unmatched url, redirect to /state1
    $urlRouterProvider
        .when('/', ['$state', function ($state) {  //为active.ejs的ui-view设置默认值
            $state.go('list');
        }])
        .otherwise("/list");

    // Now set up the states
    $stateProvider
        .state('list', {  //首页
            url: "/list",
            templateUrl: "list1",
            controller: "list.controller"
        })
        .state('info', {  //详情
            url: "/info?article_id",
            templateUrl: "info1",
            controller: "info.controller"
        })
        ;

});

//admin全局controller
adminModule.controller('global.controller', ['$scope','$http','$location',function ($scope, $http, $location) {

    console.log("global controller");


    //header 切换导航
    $("header ul.nav li").click(function(){
        var obj = $(this);

        //遍历删除cur
        $("header ul.nav li").each(function () {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
            }
        });

        //添加cur
        obj.addClass("active");

    });


}]);


adminModule.filter( "toLocaleString", function() {
    var filterfun = function(code) {
        var result = new Date(parseInt(code)).toLocaleString() ;

        return result;
    };
    return filterfun;
});
