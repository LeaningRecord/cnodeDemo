/**
 * Created by wuwanyu on 2016/6/22.
 */
adminModule.controller('list.controller', ['$scope','$state','$location','articleService',function ($scope, $state, $location,articleService) {

    console.log("in list controller");

    getArticleList(null);  //init article list

    $scope.paginationConf={
        currentPage: 1,  //当前页码
        totalItems: 400, //数据总量
        itemsPerPage: 12,  //每页显示数据
        pagesLength: 10,   //显示页码
        onChange: function(){  //数据变化时，触发
            $scope.onPageChange(this.currentPage,this.itemsPerPage);
        }
    };

    $scope.onPageChange=function(currentPage,itemsPerPage){
        console.log("change page to:"+$scope.paginationConf.currentPage);

        var params = {
            pageIdx : $scope.paginationConf.currentPage,
            pageSize : $scope.paginationConf.itemsPerPage
        };

        getArticleList(params);

    };



    $scope.toArticleInfo = function(article_id){
        $state.go('info',{article_id:article_id});
    }



    function getArticleList(params){

        console.log("params:",params);

        articleService.getArticleList(null,function(response){

            console.log("response.result:",response.result);

            $scope.list = response.result;

        });
    }

    //首页文字类型 切换导航
    $(".container ul.nav-pills li").click(function(){
        var obj = $(this);

        //遍历删除cur
        $(".container .panel-heading ul.nav-pills li").each(function () {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
            }
        });

        //添加cur
        obj.addClass("active");

    });


}]);