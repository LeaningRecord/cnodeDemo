/**
 * Created by wuwanyu on 2016/6/22.
 */

adminModule.service('articleService',['$http',function($http){

    //getList
    this.getArticleList = function(params,cb){
        angularHttpGet($http,"/cnode/api/article/getList",params,cb)


    }


    //getInfo
    this.getArticleInfo = function(params,cb){
        angularHttpGet($http,"/cnode/api/article/getInfo",params,cb)
    }


//angularJs https get������װ
    function angularHttpGet($http,url,params,callBack){
        $http({
            method  : 'GET',
            url     : url,
            params : params
        })
            .success(function(data) {
                callBack(data);
            });
    }



}] );



