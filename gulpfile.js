/*gulpfile.js*/
var gulp = require('gulp'),
    mincss = require('gulp-mini-css'),  //压缩css
    minjs = require('gulp-uglify'),  //压缩js
    concat = require('gulp-concat'),  //合并
    rename = require('gulp-rename'), //重命名
    del = require('del'); //删除


var source_css = './public/style', //css
    des_css = './public/plugins/css',
    source_js = './public/scripts',  //js
    des_js = './public/plugins/js';


//压缩合并css
gulp.task('minCss', function () {
    gulp.src(source_css+'/*.css')  //输入
        .pipe(concat("cnode.css"))  //合并
        .pipe(mincss()) //压缩css
        .pipe(rename({suffix: '.min'}))  //重命名
        .pipe(gulp.dest(des_css));  //输出
});

//合并css
gulp.task('concatCss', function () {
    gulp.src(source_css+'/*.css')  //输入
        .pipe(concat("cnode.css"))  //合并
        .pipe(gulp.dest(des_css));  //输出
});

/*
 *任务：压缩合并js
 * cnode.js = /controller/*.js + /service/*.js
 * */
gulp.task('minJs', function () {
    gulp
        .src([source_js+"/controller/*.js",source_js+"/service/*.js"])
        .pipe(concat("cnode.js"))
        .pipe(minjs())
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest(des_js));
});

/*
 *任务：合并js
 *  cnode.min.js = /controller/*.js + /service/*.js
 * */
gulp.task('concatJs', function () {
    gulp
        .src([source_js+"/controller/*.js",source_js+"/service/*.js"])
        .pipe(concat("cnode.js"))
        .pipe(gulp.dest(des_js));
});

//默认执行
gulp.task('default',function(){
    gulp.run('minCss','concatCss','minJs','concatJs');
});