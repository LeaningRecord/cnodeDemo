var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index.ejs');
});


router.get('/list1', function(req, res) {
  res.render('article/list.ejs');
});

router.get('/info1', function(req, res) {
  res.render('article/info.ejs');
});


module.exports = router;
